<?php
header('Content-Type: text/html; charset=UTF-8');
//проверяем, является ли пользователь админом для правильной отсылки и отображения (два раза, поэтому функция)
function checkadmin() {
    $user = 'user1';
    $pass = 'user1';
    $db = new PDO('mysql:host=localhost;dbname=task1', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    try {
        $stmt = $db->prepare("SELECT pwdverify FROM adminpassword WHERE login=:i");
        $result = $stmt->execute(array("i"=> $_SERVER['PHP_AUTH_USER']));
        $hashpwd = (current(current($stmt->fetchAll(PDO::FETCH_ASSOC))));
    }
    catch(PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }
    if (password_verify($_SERVER['PHP_AUTH_PW'], $hashpwd)) {
        //global $PHP_ADMIN; //кастомная переменная для исключения проверок ошибок
        return true;
    }
    else return false;
}

$ability_data = ['god', 'clip', 'fly'];
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();
  if (!empty($_COOKIE['save'])) {
    //print('Спасибо, результаты сохранены.');
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    if (!checkadmin()) $messages[] = '<div class="complete">Спасибо, результаты сохранены</div>';
    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('<div class = "complete2">Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.</div>',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }
  if (!isset($_SESSION)) { session_start(); } //без этого перестает работать (иногда)
$errors = Array();
$errors['name'] = !empty($_COOKIE['name_error']);
$errors['email'] = !empty($_COOKIE['email_error']);
$errors['year'] = !empty($_COOKIE['year_error']);
$errors['power'] = !empty($_COOKIE['power_error']);
//смотрим, кто пользователь;
if (!empty($_SERVER['PHP_AUTH_USER']) &&
    !empty($_SERVER['PHP_AUTH_PW']) && checkadmin()) {
    printf('<div class="complete3"> Редактирование данных администратором </div>');
    if (!empty($_SESSION['login'])) {
        printf('<div class="complete3"> %s, %s', $_SESSION['login'], $_SESSION['uid']);
        printf('</div>');
        printf('<div class="complete3"> Для обычного доступа к форме нажимите на кнопку выхода на странице <a href="admin.php">админа</a>   </div>');
    }
    else printf('<div class="complete3">Пользователь не выбран, сделайте выбор на странице <a href="admin.php">админа</a> или выйдите из его профиля для новой записи </div>');
}
//ошибки в полях
if ($errors['name']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('name_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error1">Заполните имя</div>';
  }
if ($errors['email']) {
  if ($errors['email']==1){
    setcookie('email_error', '', 100000);
    $messages[] = '<div class="error1">Заполните почту</div>';
  }
  else {
     setcookie('email_error', '', 100000);
     $messages[] = '<div class="error1">Неправильная почта</div>';
  }
  }
if ($errors['year']) {
    setcookie('year_error', '', 100000);
    $messages[] = '<div class="error1">Заполните правильно год</div>';
  }
if ($errors['power']) {
    setcookie('power_error', '', 100000);
    $messages[] = '<div class="error1">Выберите суперспособность</div>';
  }
$values = array();
$values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
$values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
$values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
$values['power'] = empty($_COOKIE['power_value']) ? '' : unserialize($_COOKIE['power_value']);
//print_r($values['power']);
$values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
$values['limb'] = empty($_COOKIE['limb_value']) ? '' : $_COOKIE['limb_value'];
$values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];


 // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  if ( $errors && 
  !empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])
      ) {
      //стандартная авторизация, если не админ
      if (!checkadmin()){
          printf('<div class="complete2"> Вход с логином %s, uid %s', $_SESSION['login'], $_SESSION['uid']);
          printf('</div>');
      }
        $user = 'user1';
        $pass = 'user1';
        $db = new PDO('mysql:host=localhost;dbname=task1', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
        try {
          $stmt = $db->prepare("SELECT id FROM userpassword WHERE login=:i");
          $result = $stmt->execute(array("i"=> $_SESSION['login']));
          $idbd = (current(current($stmt->fetchAll(PDO::FETCH_ASSOC))));
          $stmt1 = $db->prepare("SELECT * FROM usersuperpower WHERE id=:i");
          $result = $stmt1->execute(array("i"=> $idbd));
          $superpower = $stmt1->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e) {
            print('Error : ' . $e->getMessage());
            exit();
        } 
        try {
          $stmt = $db->prepare("SELECT * FROM userbase WHERE id=:i");
          $result = $stmt->execute(array("i"=>$idbd));
          $data = current($stmt->fetchAll(PDO::FETCH_ASSOC));
          //print_r($data);
        }
        catch(PDOException $e) {
            print('Error : ' . $e->getMessage());
            exit();
        }
        $values['name'] = filter_var($data['name'],  FILTER_SANITIZE_SPECIAL_CHARS);
        $values['email'] = filter_var($data['email'], FILTER_SANITIZE_SPECIAL_CHARS);
        $values['year'] = filter_var($data['year'],  FILTER_SANITIZE_SPECIAL_CHARS);
        $values['sex'] = $data['sex'];
        $values['limb'] = $data['limb'];
        $values['bio'] = filter_var($data['bio'], FILTER_SANITIZE_SPECIAL_CHARS);
        $abil = [];
        $q = 0;
        for ($ii = 0; $ii < count($superpower); $ii++) {
            $abil[$q] = $ability_data[$superpower[$ii][power]];
            $q++;
        }
        $values['power'] = $abil;
  }
  include('form.php');
}


else {
    $errors = FALSE;

    if (empty($_POST['name'])) {
        //print('Заполните имя.<br/>');
        setcookie('name_error', '1', time() + 24 * 60 * 60);
        $errors = true;
    } else {
        setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
    }

    if (empty($_POST['email'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        setcookie('email_error', '2', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }

    if (empty($_POST['year'])) {
        setcookie('year_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        $year = $_POST['year'];
        if (!(is_numeric($year) && intval($year) >= 1900 && intval($year) <= 2020)) {
            setcookie('year_error', '1', time() + 24 * 60 * 60);
            $errors = TRUE;
        } else {
            setcookie('year_value', $_POST['year'], time() + 30 * 24 * 60 * 60);
        }
    }

    if (empty($_POST['power'])) {
        setcookie('power_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        $abilities = $_POST['power'];
        foreach ($abilities as $ability) {
            if (!in_array($ability, $ability_data)) {
                print('Недопустимая способность<br>');
                $errors = TRUE;
            }
        }
    }
    if ($errors == false) {
        setcookie('power_value', serialize($_POST['power']), time() + 30 * 24 * 60 * 60);
        setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
        setcookie('limb_value', $_POST['limb'], time() + 30 * 24 * 60 * 60);
        setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
    }

    $ability_insert = [];
    foreach ($ability_data as $ability) {
        $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
    }

    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    } else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('name_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('year_error', '', 100000);
        setcookie('power_error', '', 100000);
    }
// Проверяем меняются ли ранее сохраненные данные или отправляются новые.
    if (!isset($_SESSION)) {
        session_start();
    }
    if (!empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])) {
        //проверка на подмену CSRF токена у авторизованного пользователя
        if (isset($_SESSION['uid']) && $_SESSION['uid'] == @$_POST['csrf_token']) {
        $user = 'user1';
        $pass = 'user1';
        $db = new PDO('mysql:host=localhost;dbname=task1', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
        try {
            $stmt = $db->prepare("SELECT id FROM userpassword WHERE login=:i");
            $result = $stmt->execute(array("i" => $_SESSION['login']));
            $idbd = (current(current($stmt->fetchAll(PDO::FETCH_ASSOC))));
            $stmt1 = $db->prepare("UPDATE userbase SET name=:name, year=:year, sex=:sex, email=:email, bio=:bio, limb=:limb WHERE id =:id");
            $stmt1->bindParam(':name', $_POST['name']);
            $stmt1->bindParam(':year', $_POST['year']);
            $stmt1->bindParam(':sex', $_POST['sex']);
            $stmt1->bindParam(':email', $_POST['email']);
            $stmt1->bindParam(':bio', $_POST['bio']);
            $stmt1->bindParam(':limb', $_POST['limb']);
            $stmt1->bindParam(':id', $idbd);
            $stmt1->execute();
            $stmt0 = $db->prepare("SELECT power FROM usersuperpower WHERE id=:i");
            $result = $stmt0->execute(array("i" => $idbd));
            $powerstemp = $stmt0->fetchAll(PDO::FETCH_ASSOC);
            $powers = [];
            $q = 0;
            foreach ($powerstemp as $element) {
                $powers[$q] = $element[power];
                $q++;
            }
            if (!empty($ability_insert['god']) && !in_array('0', $powers)) {
                $stmt2 = $db->prepare("INSERT INTO usersuperpower (id, power) VALUES (:id,:power)");
                $stmt2->bindParam(':id', $idbd);
                $stmt2->bindParam(':power', intval(0));
                $stmt2->execute();
            } else if (empty($ability_insert['god']) && in_array('0', $powers)) {
                $stmt3 = $db->prepare("DELETE FROM usersuperpower where id=:id and power=:power");
                $stmt3->bindParam(':id', $idbd);
                $stmt3->bindParam(':power', intval(0));
                $stmt3->execute();
            }
            if (!empty($ability_insert['clip']) && !in_array('1', $powers)) {
                $stmt4 = $db->prepare("INSERT INTO usersuperpower (id, power) VALUES (:id,:power)");
                $stmt4->bindParam(':id', $idbd);
                $stmt4->bindParam(':power', intval(1));
                $stmt4->execute();
            } else if (empty($ability_insert['clip']) && in_array('1', $powers)) {
                $stmt5 = $db->prepare("DELETE FROM usersuperpower where id=:id and power=:power");
                $stmt5->bindParam(':id', $idbd);
                $stmt5->bindParam(':power', intval(1));
                $stmt5->execute();
            }
            if (!empty($ability_insert['fly']) && !in_array('2', $powers)) {
                $stmt6 = $db->prepare("INSERT INTO usersuperpower (id, power) VALUES (:id,:power)");
                $stmt6->bindParam(':id', $idbd);
                $stmt6->bindParam(':power', intval(2));
                $stmt6->execute();
            } else if (empty($ability_insert['fly']) && in_array('2', $powers)) {
                $stmt7 = $db->prepare("DELETE FROM usersuperpower where id=:id and power=:power");
                $stmt7->bindParam(':id', $idbd);
                $stmt7->bindParam(':power', intval(2));
                $stmt7->execute();
            }
        } catch (PDOException $e) {
            print('Error : ' . $e->getMessage());
            exit();
        }
        if (!checkadmin()) {
            printf('<div class="complete2"> Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
            printf('</div>');
        }
    }
    else printf("CSRF атака?");
    } else {

            //пытаемся отправить новую форму под админом (исключаем, так как будут пробемы с обычной авторизацией)
            if (checkadmin() && empty($_SESSION['login'])) {
                printf('Форма для редактирования не выбрана, а новую форму под администратором заполнить нельзя. Для обычного доступа к форме нажмите на кнопку выхода на странице <a href="admin.php">админа</a>');
                exit();
            }
            // Генерируем уникальный логин и пароль.
            $login = uniqid("user");
            $pwd = rand(100000, 1000000);
            // Сохраняем в Cookies.
            setcookie('login', $login);
            setcookie('pass', $pwd);

            $user = 'user1';
            $pass = 'user1';
            $db = new PDO('mysql:host=localhost;dbname=task1', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
            try {
                $stmt = $db->prepare("INSERT INTO userbase (name,year,sex,email,bio,limb) VALUES (:name,:year,:sex,:email,:bio,:limb)");
                $stmt->bindParam(':name', $_POST['name']);
                $stmt->bindParam(':year', $_POST['year']);
                $stmt->bindParam(':sex', $_POST['sex']);
                $stmt->bindParam(':email', $_POST['email']);
                $stmt->bindParam(':bio', $_POST['bio']);
                $stmt->bindParam(':limb', $_POST['limb']);
                $stmt->execute();
                $last_id = $db->lastInsertId();
                if (!empty($ability_insert['god'])) {
                    $stmt1 = $db->prepare("INSERT INTO usersuperpower (id, power) VALUES (:id,:power)");
                    $stmt1->bindParam(':id', intval($last_id));
                    $stmt1->bindParam(':power', intval(0));
                    $stmt1->execute();
                }
                if (!empty($ability_insert['clip'])) {
                    $stmt2 = $db->prepare("INSERT INTO usersuperpower (id, power) VALUES (:id,:power)");
                    $stmt2->bindParam(':id', intval($last_id));
                    $stmt2->bindParam(':power', intval(1));
                    $stmt2->execute();
                }
                if (!empty($ability_insert['fly'])) {
                    $stmt3 = $db->prepare("INSERT INTO usersuperpower (id, power) VALUES (:id,:power)");
                    $stmt3->bindParam(':id', intval($last_id));
                    $stmt3->bindParam(':power', intval(2));
                    $stmt3->execute();
                }
                $stmt4 = $db->prepare("INSERT INTO userpassword (id, login, pwd) VALUES (:id,:login, :pwd)");
                $stmt4->bindParam(':id', intval($last_id));
                $stmt4->bindParam(':login', $login);
                $stmt4->bindParam(':pwd', password_hash($pwd, PASSWORD_DEFAULT));
                $stmt4->execute();
            } catch (PDOException $e) {
                print('Error : ' . $e->getMessage());
                exit();
            }
    }
    //если подменили/нет CSRF токена
        if (isset($_SESSION['uid']) && $_SESSION['uid'] != @$_POST['csrf_token']) {
            printf('Уходи');
            exit();
        }
        setcookie('save', '1');
        //проверяем, перешел ли пользователь в форму с админки, тогда возвращаем его обратно
        if (!empty($_SERVER['PHP_AUTH_USER']) &&
            !empty($_SERVER['PHP_AUTH_PW']) && checkadmin()) {
            header('Location: admin.php');
            exit();
        } else {
            header('Location: index.php');
            exit();
        }
}
